
1. Build the topology with the provided dot and svg files

2. Enable SSH service and connect to the oob-mgmt-server

3. Provision Symmetric EVPN topology


```
git clone https://gitlab.com/nvidia-networking/systems-engineering/poc-support/netq-tshoot-demo.git
cd netq-tshoot-demo/air/ansible
ansible-playbook -i inventories/evpn_symmetric/hosts playbooks/deploy.yml
```

4. Inject faults

```
cd; cd netq-tshoot-demo/ 
ansible-playbook bootstrap.yml
```

5. Prepare lab for cloning

```
sudo -i
echo 'ubuntu:nvidia' | sudo chpasswd
sudo passwd --expire ubuntu
rm -rf ~/.ssh/authorized_keys
history -c
```

6. Clone the Sim

Shut off the current sim and paste this in your browser replacing  with the current sim ID
https://air.nvidia.com/api/v1/simulation/autoprovision/?simulation_id=70d1b661-2bb7-4e68-9633-611c0589b5cf
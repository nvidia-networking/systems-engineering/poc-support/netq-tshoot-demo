<!-- AIR:tour -->
## Troubleshooting faults with NetQ

![](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/netq-tshoot-demo/-/raw/master/img/diagram.svg)

Troubleshoot the following issues:

1. Server01 is unable to communicate with Server02
2. Server01 is unable to communicate with Server04
3. Server01 is unable to communicate with Server05


To confirm that an issue has been fixed, run the following Ansible playbook from the `oob-mgmt-server` (username/password are ubuntu/nvidia):

```
cumulus@oob-mgmt-server:~$ cd netq-tshoot-demo/
cumulus@oob-mgmt-server:~/netq-tshoot-demo$ ansible-playbook -i hosts validate.yml
```

The following guide will describe how to troubleshoot the above issues using NetQ. 

## Fault #1

From the simulation page, click on "Launch NetQ" and enter the provided username and password to login. From the workbench header select "Validation" and create a new MLAG validation. 

![](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/netq-tshoot-demo/-/raw/master/img/fault-1.png)

Compare bond1 confgs between leaf01/02 and confirm that vlan100 doesn't exist but vlan10 does.

```
cumulus@leaf02:mgmt:~$ ifquery bond1
auto bond1
iface bond1
        bond-slaves swp1
        clag-id 1
        bridge-access 10
        mtu 9000
        bond-lacp-bypass-allow yes
        mstpctl-bpduguard yes
        mstpctl-portadminedge yes

cumulus@leaf02:mgmt:~$ logout
Connection to leaf02 closed.
cumulus@leaf01:mgmt:~$ ifquery bond1
auto bond1
iface bond1
        bond-lacp-bypass-allow yes
        bond-slaves swp1
        bridge-access 100
        clag-id 1
        mstpctl-bpduguard yes
        mstpctl-portadminedge yes
        mtu 9000

cumulus@leaf01:mgmt:~$ ifquery vlan100
error: main exception: cannot find interfaces: vlan100
cumulus@leaf01:mgmt:~$ ifquery vlan10
auto vlan10
iface vlan10
        address 10.1.10.2/24
        address-virtual 00:00:00:00:00:10 10.1.10.1/24
        vlan-id 10
        vlan-raw-device bridge
        vrf RED
```

### Solution

Run the following command on leaf01
```bash
net del bond bond1 bridge access 100
net add bond bond1 bridge access 10
net commit
```


## Fault #2

From the main workbench header, select "Card" and open the "EVPN Sessions" card. Open this card in a full-screen view and look at the "All Sessions" screen:

![](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/netq-tshoot-demo/-/raw/master/img/fault-2.png)

### Solution

Run the following command on leaf01 and leaf02
```bash
sudo ip link del vni10
sudo sed -i 's/vxlan-id 100/vxlan-id 10/' /etc/network/interfaces
sudo ifreload -a
```



## Fault #3

From the main workbench header, select "Validation" and create a new on-demand EVPN validation:

![](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/netq-tshoot-demo/-/raw/master/img/fault-3.png)

### Solution

Run the following commands on leaf03 and leaf04
```bash
net del vlan 4001 vrf BLUE
net add vlan 4001 vrf RED           
net del vlan 4002 vrf RED
net add vlan 4002 vrf BLUE
net commit
```



<!-- AIR:tour -->
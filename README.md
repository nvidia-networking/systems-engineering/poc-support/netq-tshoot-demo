## Troubleshooting faults with NetQ

## Requirements

Ansible 2.9+ needs to be installed:

```
sudo apt update 
sudo apt install python3-pip -y
sudo python3 -m pip install ansible~=2.9
```

## Prerequisited

1. Deploy CITC topology in [air](air.nvidia.com) with "EVPN Symmetric" configurations applied.
2. Connect to the OOB server and git-clone this repo.
3. Inject all faults with `ansible-playbook bootstrap.yml`.
4. Run `ansible-playbook -i hosts validate.yml` to validate what faults have been resolved.

## Fault #1

Create on-demand MLAG validation. Check what the error is saying.

![](img/fault-1.png)

Compare bond1 confgs between leaf01/02 and confirm that vlan100 doesn't exist but vlan10 does.

```
cumulus@leaf02:mgmt:~$ ifquery bond1
auto bond1
iface bond1
        bond-slaves swp1
        clag-id 1
        bridge-access 10
        mtu 9000
        bond-lacp-bypass-allow yes
        mstpctl-bpduguard yes
        mstpctl-portadminedge yes

cumulus@leaf02:mgmt:~$ logout
Connection to leaf02 closed.
cumulus@leaf01:mgmt:~$ ifquery bond1
auto bond1
iface bond1
        bond-lacp-bypass-allow yes
        bond-slaves swp1
        bridge-access 100
        clag-id 1
        mstpctl-bpduguard yes
        mstpctl-portadminedge yes
        mtu 9000

cumulus@leaf01:mgmt:~$ ifquery vlan100
error: main exception: cannot find interfaces: vlan100
cumulus@leaf01:mgmt:~$ ifquery vlan10
auto vlan10
iface vlan10
        address 10.1.10.2/24
        address-virtual 00:00:00:00:00:10 10.1.10.1/24
        vlan-id 10
        vlan-raw-device bridge
        vrf RED
```

<details>
<summary>solution</summary>
<p>

Run the following command on leaf01
```bash
net del bond bond1 bridge access 100
net add bond bond1 bridge access 10
net commit
```

</p>
</details>  

## Fault #2

Open the NetworkServices/EVPN card and look at all sessions

![](img/fault-2.png)

<details>
<summary>solution</summary>
<p>

Run the following command on leaf01 and leaf02
```bash
sudo ip link del vni10
sudo sed -i 's/vxlan-id 100/vxlan-id 10/' /etc/network/interfaces
sudo ifreload -a
```

</p>
</details>  

## Fault #3

Run EVPN validation and observe VRF consistency check failure

![](img/fault-3.png)

<details>
<summary>solution</summary>
<p>

Run the following commands on leaf03 and leaf04
```bash
net del vlan 4001 vrf BLUE
net add vlan 4001 vrf RED           
net del vlan 4002 vrf RED
net add vlan 4002 vrf BLUE
net commit
```

</p>
</details>  